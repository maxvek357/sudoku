import { getRandomInt } from "../../helpers/getRandomInt"
import { shuffleArray } from "../../helpers/shuffle"

const baseNumbers = Array.from({ length: 9 }, (_, i) => i + 1)
const posNumbers = Array.from({ length: 9 }, (_, i) => i)

const fillArr = () => {
  const arr = []
  for (let i = 0; i < 9; i++) {
    arr.push({
      id: i,
      name: '',
      numbers: Array.from({ length: 9 }, () => 0),
      pens: [
        [], [], [], [], [], [], [], [], []
      ]
    })
  }
  return arr
}

const gameArr = fillArr()

const firstFill = () => {
  gameArr[0].numbers = shuffleArray<number>([...baseNumbers])
  gameArr[4].numbers = shuffleArray<number>([...baseNumbers])
  gameArr[8].numbers = shuffleArray<number>([...baseNumbers])
}

const fill1stRow = () => {
  for (let i = 0; i < 1; i++) {

  }
}

const clearRow = (arr: number[], idx: number, cNumb: number) => {
  const ridx = gameArr[idx].numbers.indexOf(cNumb)
  if (ridx > 2) {
    if (ridx < 6) {
      arr.splice(3, 3)
    } else {
      arr.splice(6)
    }
  } else {
    arr.splice(0, 3)
  }
}

const clear2ndRow = (arr: number[], idx: number, cNumb: number) => {
  const ridx2 = gameArr[idx].numbers.indexOf(cNumb)
  if (ridx2 > 2) {
    if (ridx2 < 6) {
      arr.splice(0, 3)
    } else {
      arr.splice(3, 3)
    }
  } else {
    arr.splice(0, 3)
  }
}

const clearCol = (arr: number[], idx: number, cNumb: number) => {
  const cidx = gameArr[idx].numbers.indexOf(cNumb)
  if (cidx > 2) {
    if (cidx < 6) {
      if (arr.includes(cidx)) arr.splice(arr.indexOf(cidx), 1)
      if (arr.includes(cidx + 3)) arr.splice(arr.indexOf(cidx + 3), 1)
      if (arr.includes(cidx - 3)) arr.splice(arr.indexOf(cidx - 3), 1)
    } else {
      if (arr.includes(cidx)) arr.splice(arr.indexOf(cidx), 1)
      if (arr.includes(cidx - 3)) arr.splice(arr.indexOf(cidx - 3), 1)
      if (arr.includes(cidx - 6)) arr.splice(arr.indexOf(cidx - 6), 1)
    }
  } else {
    if (arr.includes(cidx)) arr.splice(arr.indexOf(cidx), 1)
    if (arr.includes(cidx + 3)) arr.splice(arr.indexOf(cidx + 3), 1)
    if (arr.includes(cidx + 6)) arr.splice(arr.indexOf(cidx + 6), 1)
  }
}

const fill12Box = () => {
  const occPos: number[] = []
  for (let i = 1; i < 10; i++) {
    let ca = [...posNumbers]
    clearRow(ca, 0, i)
    clearCol(ca, 4, i)
    console.log('i', i, ca);

    ca = ca.filter(p => !occPos.includes(p))
    console.log(ca);
    const pos = ca[getRandomInt(ca.length)]
    occPos.push(pos)
    console.log(occPos, pos);
    gameArr[1].numbers[pos] = i
  }
}

const fill13Box = () => {
  const ca = [...baseNumbers]
  clearRow(ca, 0, 1)
  clear2ndRow(ca, 1, 1)
  clearCol(ca, 8, 1)
  const pos = ca[getRandomInt(ca.length)]
  gameArr[2].numbers = Array.from({ length: 9 }, (_, i) => i === pos ? 1 : 0)
}

const fill21Box = () => {
  const ca = [...baseNumbers]
  const ridx = gameArr[4].numbers.indexOf(1)
  if (ridx > 2) {
    if (ridx < 5) {
      ca.splice(3, 3)
    } else {
      ca.splice(6)
    }
  } else {
    ca.splice(0, 3)
  }
  const cidx = gameArr[0].numbers.indexOf(1) + 1

  if (cidx > 4) {
    if (cidx > 3 && cidx < 7) {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
      if (ca.includes(cidx - 6)) ca.splice(ca.indexOf(cidx - 6), 1)
    }
  } else {
    if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
    if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
    if (ca.includes(cidx + 6)) ca.splice(ca.indexOf(cidx + 6), 1)
  }
  const pos = ca[getRandomInt(ca.length)] - 1
  gameArr[3].numbers = Array.from({ length: 9 }, (_, i) => i === pos ? 1 : 0)
}

const fill23Box = () => {
  const ca = [...baseNumbers]
  const ridx = gameArr[3].numbers.indexOf(1)
  if (ridx > 2) {
    if (ridx < 5) {
      ca.splice(3, 3)
    } else {
      ca.splice(6)
    }
  } else {
    ca.splice(0, 3)
  }
  const ridx2 = gameArr[4].numbers.indexOf(1)
  if (ridx2 > 2) {
    if (ridx < 2) {
      ca.splice(3, 3)
    } else {
      ca.splice(0, 3)
    }
  } else {
    ca.splice(0, 3)
  }
  const cidx = gameArr[8].numbers.indexOf(1) + 1
  if (cidx > 4) {
    if (cidx > 3 && cidx < 7) {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
      if (ca.includes(cidx - 6)) ca.splice(ca.indexOf(cidx - 6), 1)
    }
  } else {
    if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
    if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
    if (ca.includes(cidx + 6)) ca.splice(ca.indexOf(cidx + 6), 1)
  }
  const cidx2 = gameArr[2].numbers.indexOf(1) + 1
  if (cidx2 > 4) {
    if (cidx2 > 3 && cidx2 < 7) {
      if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
      if (ca.includes(cidx2 - 6)) ca.splice(ca.indexOf(cidx2 - 6), 1)
    }
  } else {
    if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
    if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
    if (ca.includes(cidx2 + 6)) ca.splice(ca.indexOf(cidx2 + 6), 1)
  }
  gameArr[5].numbers = Array.from({ length: 9 }, (_, i) => i === ca[0] - 1 ? 1 : 0)
}

const fill31Box = () => {
  const ca = [...baseNumbers]
  const ridx = gameArr[8].numbers.indexOf(1)
  if (ridx > 2) {
    if (ridx < 5) {
      ca.splice(3, 3)
    } else {
      ca.splice(6)
    }
  } else {
    ca.splice(0, 3)
  }
  const cidx = gameArr[0].numbers.indexOf(1) + 1
  if (cidx > 4) {
    if (cidx > 3 && cidx < 7) {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
      if (ca.includes(cidx - 6)) ca.splice(ca.indexOf(cidx - 6), 1)
    }
  } else {
    if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
    if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
    if (ca.includes(cidx + 6)) ca.splice(ca.indexOf(cidx + 6), 1)
  }
  const cidx2 = gameArr[3].numbers.indexOf(1) + 1
  if (cidx2 > 4) {
    if (cidx2 > 3 && cidx2 < 7) {
      if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
      if (ca.includes(cidx2 - 6)) ca.splice(ca.indexOf(cidx2 - 6), 1)
    }
  } else {
    if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
    if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
    if (ca.includes(cidx2 + 6)) ca.splice(ca.indexOf(cidx2 + 6), 1)
  }
  const pos = ca[getRandomInt(ca.length)] - 1
  gameArr[6].numbers = Array.from({ length: 9 }, (_, i) => i === pos ? 1 : 0)
}

const fill32Box = () => {
  const ca = [...baseNumbers]
  const ridx = gameArr[8].numbers.indexOf(1)
  if (ridx > 2) {
    if (ridx < 5) {
      ca.splice(3, 3)
    } else {
      ca.splice(6)
    }
  } else {
    ca.splice(0, 3)
  }
  const ridx2 = gameArr[6].numbers.indexOf(1)
  if (ridx2 > 2) {
    if (ridx < 2) {
      ca.splice(3, 3)
    } else {
      ca.splice(0, 3)
    }
  } else {
    ca.splice(0, 3)
  }
  const cidx = gameArr[1].numbers.indexOf(1) + 1
  if (cidx > 4) {
    if (cidx > 3 && cidx < 7) {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
      if (ca.includes(cidx - 3)) ca.splice(ca.indexOf(cidx - 3), 1)
      if (ca.includes(cidx - 6)) ca.splice(ca.indexOf(cidx - 6), 1)
    }
  } else {
    if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx), 1)
    if (ca.includes(cidx + 3)) ca.splice(ca.indexOf(cidx + 3), 1)
    if (ca.includes(cidx + 6)) ca.splice(ca.indexOf(cidx + 6), 1)
  }
  const cidx2 = gameArr[4].numbers.indexOf(1) + 1
  if (cidx2 > 4) {
    if (cidx2 > 3 && cidx2 < 7) {
      if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
    } else {
      if (ca.includes(cidx)) ca.splice(ca.indexOf(cidx2), 1)
      if (ca.includes(cidx2 - 3)) ca.splice(ca.indexOf(cidx2 - 3), 1)
      if (ca.includes(cidx2 - 6)) ca.splice(ca.indexOf(cidx2 - 6), 1)
    }
  } else {
    if (ca.includes(cidx2)) ca.splice(ca.indexOf(cidx2), 1)
    if (ca.includes(cidx2 + 3)) ca.splice(ca.indexOf(cidx2 + 3), 1)
    if (ca.includes(cidx2 + 6)) ca.splice(ca.indexOf(cidx2 + 6), 1)
  }
  gameArr[7].numbers = Array.from({ length: 9 }, (_, i) => i === ca[0] - 1 ? 1 : 0)
}

firstFill()

fill12Box()
fill13Box()

fill21Box()
fill23Box()

fill31Box()
fill32Box()

console.log(gameArr)

const getGameArr = () => gameArr

export { gameArr, getGameArr }