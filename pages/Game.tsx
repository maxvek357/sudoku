import classes from '../styles/game.module.scss'
import clsx from 'clsx'
import { gameArr, getGameArr } from './api/game'
import { useState } from 'react'

const Game = () => {
  const [gameBoard, setGameBoard] = useState(gameArr)

  const handleClick = () => {
    console.log(getGameArr());
    
    setGameBoard([...getGameArr()])
  }

  return (
    <div className={classes.box}>
      <h1 onClick={handleClick}>Game</h1>
      <div className={classes.gameField}>
        {gameBoard.map((box, i) =>
          <div className={classes.gameBox} key={i}>
            {box.numbers.map((numb, j) =>
              <div className={clsx(classes.gameBox, classes.gameBoxSmall)} key={j}>
                {numb
                  ? <span className={classes.gameNumber}>{numb}</span>
                  : box.pens[j].map((pen, k) =>
                    <div className={clsx(classes.gameBox, classes.gameBoxPen)} key={k}>
                      <span className={clsx(classes.gameNumber, classes.gameNumberPen)}>{pen}</span>
                    </div>
                  )}
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  )
}

export default Game