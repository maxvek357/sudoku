import { NextPage } from 'next'
import Game from './Game'

const Home: NextPage = () => {
  return (
    <Game></Game>
  )
}

export default Home
